---
home: true
heroImage: /hero.jpeg
features:
- title: Learn continuously
  details: It doesn't matter what you learn, just make sure you're growing.
- title: Contribute
  details: Use what you learn to provide value to others.
- title: Don't stop
  details: Don't get stuck in a rut, and always keep your eyes open for new opportunities.
---