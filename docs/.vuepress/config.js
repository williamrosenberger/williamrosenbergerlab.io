module.exports = {
    title: 'Will Rosenberger',
    description: "Ramblings.",
    base: '/',
    dest: 'public',
    themeConfig: {
        search: false,
        nav: [
            { text: 'Home', link: '/' },
            { text: 'About', link: '/about/' },
            { text: 'Blog', link: '/blog/' },
        ],
        sidebar: [
            '/',
            '/about/',
            '/blog/'
        ]
    }
}