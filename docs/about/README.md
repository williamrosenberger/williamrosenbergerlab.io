# About Me

By trade, I'm a full-stack developer (web development since 2015). However, I get my greatest sense of joy when building new technical skills and pursuing new interests. This blog is my attempt at organizing my rambling path through these interests so that I can better understand the things that I find most useful. The ultimate goal here is to discover areas where I can have an impact.

**My assumption**: I know nothing, but can learn anything.

# Education

**Bachelor of Science** in **Computer Science**. **Minor** in Mathematics.

# Experience

I have been working for the same company since a year before my graduation (started work in the summer of 2015). Although my job began with pretty simple web development, my company works on a huge range of projects that have given me the opportunity to build experience and expertise in a huge range of areas. I couldn't have asked for a better job right out of college.

I started out doing back- and front-end development for this company. This has spread into system administration, cloud administration, data analysis, and project management. I am responsible for mentoring students and early-career staff.

My interest in software started when I was 12 years old with building Lego Mindstorms robots and Roblox games.

Prior to beginning "real" programming work, I worked on a small drill rig for a construction material testing company. So, I'm happy to say my entire life hasn't been spent behind a keyboard! (though, I guess there's nothing wrong with that)
