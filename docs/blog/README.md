---
sidebar: auto
---

# Blog posts

This page contains quick projects I've experimented with. I know, this isn't a real blog, it's just a simple page. Someday, I'll likely experiment with getting "real" blogging set up (following, for example, [BenCodeZen's boilerplate](https://github.com/bencodezen/vuepress-blog-boilerplate)), but this single page approach should work well for now. Life's all about continuous improvement!

## A minimally useful setup.py file

Several years ago, when I was first getting into the depths of Python programming, I distinctly
remember hammering my head against the same problem over and over again: `import` statements never
seemed to do what I wanted them to do. They'd work great in the early stages of the project,
when the module and file structure was fairly flat. However, at some point, the project would always
reach the point of needing to be reorganized from the flat file structure to something easier to
navigate - for example, moving any callable entrypoint scripts (`if __name__ == "__main__"` type
stuff) into a `bin` directory, and moving any helper functions into other neighboring directories. Every
time I did this, though, even after changing my code to take the new file structure into account,
my `import`s would completely fail.

Of course, being a good little programmer, every time I encountered this problem, I would spend time
trying to track down a solution on Google. I'd find plenty of Stack Overflow questions and answers
muttering things about `PYTHONPATH` and `PYTHONHOME` environment variables, but they'd always end
with something to the effect of "Just use `setuptools`". So, I'd look up how to use `setuptools`. And,
being the inexperienced little programmer that I was (am? am.), and given that I would usually only have a few
minutes to invest in research and refactoring, I'd immediately run for
the hills. There's a _huge_ amount of capability in the `setuptools` library, and, given the time limits, I
thought it was ridiculous to have to learn an entire new ecosystem just to get `import`
statements to work properly. So, I would end up just reverting back to the old flat package hierarchy so that
I could get on with my real work.

Eventually, after enough iterations of this cycle, and reading enough blog posts and tutorials, I found just the
right pieces of the `setuptools` ecosystem I needed in order to fix the `import` issues _without_ writing a
full blown PyPI package. And, it's actually _super_ simple. Now, whenever I start a new non-trivial project,
my first 5 minutes are taken up by creating the directory layout described in this post.

### How do `import` statements work?

Before getting into the (really simple) `setuptools` script, it's worth first discussing some of the pieces
of information I didn't fully understand when facing my `import` problem. Specifically, there's the `PYTHONHOME`,
`PYTHONPATH`, virtual environments, and how `setuptools` cooperates with all of them.

`PYTHONHOME` and `PYTHONPATH` are environment variables that define where `import` statements should look
for the objects you're trying to import. `PYTHONHOME` contains the path of where the standard libraries
for your installation of Python are located (this is where `import` would find things like `sys`, `os`,
`random`, ...). `PYTHONPATH` contains a list of directories where `import` should look for extra packages
that you've installed. Python virtual environments are a standard way of having per-project installations
of Python packages. Essentially, a virtual environment will override `pip` so that any packages you install
are saved within a special directory dedicated to whatever virtual environment is currently activated.
It then adds new values to your `PYTHONHOME` and `PYTHONPATH` variables so that Python can find
these new packages. To see this, write
a simple script to inspect the values of `sys.executable` (this is `PYTHONHOME`) and `sys.path` (this is
`PYTHONPATH`). If you run this script with and without a virtual environment enabled, you'll see these
values change.

So, in theory, all you need to do in order to get `import` statements to work correctly is to load the
package you're developing into the same virtual environment that defines the dependencies for that package.
If you do that, then, when you activate your environment, all the custom scripts you create will suddenly be
available on your `PYTHONPATH`, meaning `import` will be able to find them.

That's what `setuptools` does for you. By writing a simple `setup.py` script, you can make it so `pip` can install
your entire package into your current environment.

### The broken version of things

First, let's set up a simple project with broken import statements.

Suppose we have the following directory structure:

```
project
└── src
    ├── bin
    │   ├── __init__.py
    │   └── main.py
    └── speak
        ├── __init__.py
        └── say_hello.py
```

The `speak.say_hello` module contains a single function (`say_hello()`) that simply prints "Hello world".

The `bin/` directory is meant to contain the entrypoint scripts that users should be able to
call directly. The contents of main are:

``` python
from speak.say_hello import say_hello

def main():
    say_hello()

if __name__ == "__main__":
    main()
```

Note the `import` line. We're importing a module from within the `speak` package without specifying exactly where in the project 
structure `speak` lives. That's our goal; if things are working correctly, we should be able to import any package within `src`.

So, let's see if this version of things works. If you run `python3 src/bin/main.py`, you'll get a
`ModuleNotFoundError: No module named 'speak'` error. This is because Python can't find `speak` on `PYTHONPATH`.

### Using setuptools

To fix that exception, we need to add a special script called `setup.py` within the `src/` directory:

```
project
└── src
    ├── bin
    │   ├── __init__.py
    │   └── main.py
    ├── speak
    │   ├── __init__.py
    │   └── say_hello.py
    └── setup.py 
```

`setup.py` is the interface that allows you to describe your package to `pip` so that it can properly
update your `PYTHONPATH`.

The minimal contents I usually use for `setup.py` are:

``` python
from setuptools import setup, find_packages

setup(
    # Package meta data
    name="my_package",
    version="0.0.1",
    description="A simple library",
    # Look within this script's directory for
    # directories that contain an `__init__.py`
    # file. Install these as packages.
    packages=find_packages(),
    # Other dependencies to bring in from PyPI.
    install_requires=[
        'pytest'
    ],
    # Entrypoint scripts that should be runnable
    # from the command line.
    entry_points={
        "console_scripts": [
            "say_hello=bin.main:main"
        ]
    }
)
```

That's it! That's all you need to do in order to teach `pip` how to install
the average local Python package.

Now, how do we actually get `pip` to use this script?

First, create a new Python virtual environment and activate it by `cd`ing to your `project/` directory and
running:

``` bash
$ python3 -m venv env
$ source env/bin/activate
```

Now, as long as your virtual environment is activated, everything will be installed within that directory,
which will keep you from polluting your global environment.

To install your package, simply run:

``` bash
$ pip install -e src/
```

This causes `pip` to look within the `src/` directory for a file called `setup.py`, which it then
uses to install your package within your virtual environment.

The `-e` flag is important for development. It causes `pip` to install the package in `editable` mode,
which means any edits you make to your original scripts will take effect without requiring you to
reinstall your package.

If you try running `python3 src/bin/main.py` again, you should see the correct "Hello world" message
without any errors! Our `setup.py` script is working correctly. Moreover, because we added `entry_points`
to our setup script, runing `say_hello` at the command line is exactly equivalent to running `python3 src/bin/main.py`.

Again, there's a long list of features that can be used with `setuptools`. However, the above simple `setup.py` script
has been enough to cover the majority of my needs. And, when used in conjunction with `Makefile`s, this can
be a fantastic way of building complex packages without having to make things more complicated for anyone else
on your team.

## Integrating automated spellchecking into GitLab Pages

I'm bad at words. I need to get some form of spellchecking in place for this site, like, right now.

### Overview

This site is written using [VuePress](https://vuepress.vuejs.org/) and is hosted using [GitLab Pages](https://about.gitlab.com/product/pages/). If you aren't familiar: VuePress is a framework that converts Markdown into fully-featured, navigable web pages, and GitLab Pages is a wonderful feature of GitLab that allows you to host static sites extremely easily. So, we use VuePress to convert Markdown into a nice static webpage, then we use GitLab Pages to host that static HTML.

The original deployment pipeline for this is shown below.

![Original GitLab Pages deployment pipeline](/blog1/pipeline-before.png)

::: tip
If you're interested in this, you can get everything up to this point running in about 5 minutes by just forking [this template](https://gitlab.com/pages/vuepress).
:::

### The problem

The problem I'm encountering with this workflow is that it's taking too much time to review my spelling. Because I'm writing in Markdown, I don't have any sort of Office-like tool looking over my shoulder to make sure I'm doing my due diligence. And, even if were to copy-paste everything over to MS Word and review my spelling there, I'd still get spelling errors from any code blocks (e.g., `printf("Hello World");` would kick off the spell checker).

What I'd like to do is add a "spellcheck" job to the GitLab CI deployment pipeline. This way, the spellcheck will always be run automatically, and I'll still have the ability to make changes to the webpage on any device.

You may be asking, "Why not just download a spellcheck plugin for your editor?" Good point. That would be an easy solution. However, I've learned that one of the _really_ nice things about keeping projects closely aligned with Continuous Integration is that you're able to make changes from anywhere with zero configuration. Suppose I'm visiting my family's home and forget my laptop (Gah!). Because this site is deployed using GitLab CI/CD, I can log into GitLab from any machine and make whatever changes I need from the web-based IDE. No need to install Yarn and Node, no need to clone the repo. Just make the changes and get on with life. So, I'd like to make sure that whatever spellchecking solution I use is closely aligned with CI/CD so that I can use it from anywhere.

### Solution

A quick search for markdown spellcheckers brings up an NPM package called [markdown-spellcheck](https://www.npmjs.com/package/markdown-spellcheck). Documentation is solid enough, and it seems like it has the needed features. Only concern is that it's [pretty much unmaintained](https://github.com/lukeapage/node-markdown-spellcheck/issues/122), but that shouldn't be a problem for the purposes of this website. Let's use it.

I first installed `markdown-spellcheck`:

```
$ yarn add markdown-spellcheck
```

Then, I added the following command to my `package.json`:
``` JSON
{
  ...
  "scripts": {
    ...
    "spellcheck": "mdspell --report --ignore-numbers --ignore-acronyms --en-us '**/*.md' '!**/node_modules/**/*.md'"
  },
  ...
}
```

That's all that is needed to run the spellchecker locally. I can check all the markdown documents with: `yarn spellcheck`.

All that's left is to add this to the deployment pipeline.

I added the following job definition to my `.gitlab-ci.yml` file:

``` yaml
...
spellcheck:
  cache:
    paths:
    - node_modules/
  
  script:
  - yarn install
  - yarn run spellcheck > spellcheck.txt

  allow_failure: true
  artifacts:
    when: on_failure
    paths:
    - spellcheck.txt
...
```

There you have it! Every time you push new commits, GitLab CI/CD will run the `spellcheck` job, which will upload an artifact called `spellcheck.txt` to GitLab for review! The modified deployment pipeline looks like:

![GitLab Pages deployment pipeline after spellcheck addition](/blog1/pipeline-after.png)

### Description of the job

That's it, that's all I needed to do to add spellchecking to my deployment pipeline. However, for anyone interested, I should probably add a bit more description about the job definition.

First, let's walk through the actual spell check command: `mdspell --report --ignore-numbers --ignore-acronyms --en-us '**/*.md' '!**/node_modules/**/*.md'`.

Here, the `--report` flag tell `mdspell` that it should not try to interact with the user. Instead, it will just print a static description of the errors to `stdout`. `--ignore-numbers` and `--ignore-acronyms` are pretty self-explanatory. `--en-us` tells `mdspell` to use the US English dictionary. `'**/*.md'` means it should recursively scan the current directory for any markdown files (so, we'll be spellchecking the _entire_ site). `'!**/node_modules/**/*.md'` tells the spellchecker to _ignore_ any Markdown files that were downloaded as part of an NPM dependency.

Full documentation for `mdspell` can be found [here](https://www.npmjs.com/package/markdown-spellcheck).

Now, what about the CI/CD job definition?

The `cache` section asks GitLab to save the NPM dependencies between pipeline executions. So, we shouldn't need to reinstall all the dependencies every time we make a change.

The `script` section contains the actual code that is run:

``` yaml
script:
  - yarn install
  - yarn run spellcheck > spellcheck.txt
```

`yarn install` asks `yarn` to update the dependencies. Note that, because we're using cache, `yarn` will likely already have all the dependencies it needs after the first time we run the pipeline, so this command should run reasonable quickly.

`yarn run spellcheck > spellcheck.txt` runs the spellchecker and saves the output to `spellcheck.txt`.

`allow_failure: true` means we still want to continue running the pipeline even if the spellchecker notices a problem. So, we will still allow the website to be deployed even if there are spelling errors.

The `artifacts` section is responsible for uploading the spellcheck report:

``` yaml
artifacts:
    when: on_failure
    paths:
    - spellcheck.txt
```

Here, `when: on_failure` means we only want to upload the spellcheck report if `mdspell` noticed a misspelled word. When `mdspell` notices an incorrect word, it exits with a non-zero exit status, which is the signal to GitLab CI/CD that something went wrong.

The `paths` section is simply a listing of the files that should be uploaded.

### Issues

The biggest issue I'm running into so far is that `mdspell` highlights misspelled words in red-colored text. This doesn't work well in plain text artifact files (or GitLab CI/CD terminal output) because the color characters are not retained. There's currently an [MR open](https://github.com/lukeapage/node-markdown-spellcheck/pull/114) that apparently resolves this, but it doesn't look like the [project is currently maintained](https://github.com/lukeapage/node-markdown-spellcheck/issues/122), so it's unlikely that will be merged any time soon. If this ends up being a major problem, I'll probably look into ways to resolve this on my end, but this meets my needs for now.

### Resources

- The MR related to this blog post: [https://gitlab.com/williamrosenberger/williamrosenberger.gitlab.io/merge_requests/1/diffs](https://gitlab.com/williamrosenberger/williamrosenberger.gitlab.io/merge_requests/1/diffs).
